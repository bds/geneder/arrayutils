# ArrayUtils

## Table of contents
* [Introduction](#introduction)
* [Content](#content)
* [License](#license)

## Introduction
This repository contains the code behind the R package `ArrayUtils` that contains functions to analyze expression data from various platforms (including Affymetrix, Illumina and Agilent). The code consists of R scripts that rely on established R and BioConductor packages that focuses on the analysis of expression data such as `affy`. The current package offers high-level interfaces to these packages.

## Content
The following functionalities are available:
- Run quality control (platform specific methods)
- Pre-process data (platform specific methods)
- Batch effect correction
- Predict biological sex from expression data
- Differential expression analysis

## License
The code is available under the MIT License.